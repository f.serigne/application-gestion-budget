
const balance = document.querySelector<HTMLParagraphElement>("#balance");
const inflow = document.querySelector<HTMLParagraphElement>("#income");
const outflow = document.querySelector<HTMLParagraphElement>("#expense");
const list = document.querySelector<HTMLUListElement>("#list");
const form = document.querySelector<HTMLFormElement>("form");
const text = document.querySelector<HTMLInputElement>("#text");
const amount = document.querySelector<HTMLInputElement>("#amount");
const bouton=document.querySelector("#btn");

// Get transactions from local storage
const localStorageTransactions = JSON.parse(
  newFunction()
);

let transactions =
  localStorage.getItem("transactions") !== null ? localStorageTransactions : [];

function newFunction(): string {
  const newLocal = localStorage.getItem("transactions");
  return newLocal!;
}

// Add transaction
function addTransaction(e:any) {
  e.preventDefault();

  if (text!.value.trim() === "" || amount!.value.trim() === "") {
    document.getElementById("error_msg")!.innerHTML =
      "<span >Error: Please enter description and amount!</span>";
    setTimeout(
      () => (document.getElementById("error_msg")!.innerHTML = ""),
      5000
    );
  } else {
    const transaction = {
      id: generateID(),
      text: text!.value,
      amount: +amount!.value,
    };

    transactions.push(transaction);

    addTransactionDOM(transaction);

    updateValues();

    updateLocalStorage();

    text!.value = "";
    amount!.value = "";
  }
}

// Generate random ID
function generateID() {
  return Math.floor(Math.random() * 100000000);
}

// Transactions history
function addTransactionDOM(transaction: { id: any; text: any; amount: any; }) {
  // Get sign
  const sign = transaction.amount < 0 ? "-" : "+";

  const item = document.createElement("li");

  // Add class based on value
  item.classList.add(transaction.amount < 0 ? "minus" : "plus");

  item.innerHTML = `
    ${transaction.text} ${sign}${Math.abs(
    transaction.amount
  )} <button class="delete-btn" onclick="removeTransaction(${
    transaction.id
  })">X</button>
  `;

  list?.appendChild(item);
}

// Update the balance, inflow and outflow
function updateValues() {
  const amounts = transactions.map((transaction: { amount: any; }) => transaction.amount);

  const total = amounts.reduce((bal: any, value: any) => (bal += value), 0).toFixed(2);

  const income = amounts
    .filter((value: number) => value > 0)
    .reduce((bal: any, value: any) => (bal += value), 0)
    .toFixed(2);

  const expense =
    amounts
      .filter((value: number) => value < 0)
      .reduce((bal: any, value: any) => (bal += value), 0) * -(1).toFixed(2);

  balance!.innerText = `${total}€`;
  inflow!.innerText = `${income}€`;
  outflow!.innerText = `${expense}€`;
}

// Remove transaction by ID
function removeTransaction(id: any) {
  transactions = transactions.filter((transaction: { id: any; }) => transaction.id !== id);

  updateLocalStorage();

  start();
}

// Update local storage transactions
function updateLocalStorage() {
  localStorage.setItem("transactions", JSON.stringify(transactions));
}

// Start app
function start() {
  list!.innerHTML = "";
  transactions.forEach(addTransactionDOM);
  updateValues();
}

start();

form?.addEventListener("submit", addTransaction);

function getItem(): ((this: any, key: string, value: any) => any) | undefined {
  throw new Error("Function not implemented.");
}
