### Maquette 
![maquette](AppBudget.png)

Il s’agit ici d’une application dotée de fonctionnalités basiques destinée à la gestion de budget. Toutefois,il est possible de l’approfondir en rajoutant des fonctionnalité si on le souhaite. Elle permet de connaître en permanence l’argent qui rentre l’argent qui sort et l’historique des opérations effectuées. Grace à cette application, on peut savoir si le solde est positif ou négatif.

Je l’ai  conçu de façon simple avec un certain nombre de fonctions telles addTransaction, updateValues, removeTransaction, updateLocalStorage,  start, getItem ; ds boucles, des filtres, des containers etc...en m’appuyant sur les cours en HTML, CSS JavaScript et quelques recherches personnelles effectuées sur le net .
                                                             

